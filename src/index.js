import ReactDOM from 'react-dom';
import { BrowserRouter, Routes, Route } from "react-router-dom";

import BlogList from './blog_module/BlogList';
import BlogDetail from './blog_module/BlogDetail';

import './index.css';
import Fees from './form/Fees';

ReactDOM.render(
  <BrowserRouter>
  <Routes>
    <Route path="/" element={<Fees/>} />
      <Route path="blog" element={<BlogList />} />
      <Route path="blogdetail" element={<BlogDetail />} />
  </Routes>
</BrowserRouter>,
  document.getElementById('root')
);
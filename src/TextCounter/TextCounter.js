import React, {useState} from 'react';
import './TextCounter.css';

function TextCounter(props){
    const [enterText, setEnterText] = useState('');    
    const textChangeHandler = (event) => {
        setEnterText(event.target.value);
    }

    const [getimage, getSetImage] = useState(null);
    const imgChangeHandler = (event) => {
        if (event.target.files && event.target.files[0]) {
            getSetImage(URL.createObjectURL(event.target.files[0]));
        }
    }

    const [getValue, setGetValue] = useState('No preview available');    
    const submitHandler = () =>{
        setGetValue(enterText);
    }

    return(
        <div>
            <textarea value={enterText} rows={10} cols={100} onChange={textChangeHandler} placeholder="Enter Your Message"></textarea>
            <input type='file' onChange={imgChangeHandler} />
            <button onClick={submitHandler}>Submit</button>
            <div className="">
                <div className=""><strong>Text Counter is:</strong> {enterText.length}</div>
                <div className=""><strong>Word Counter is:</strong>{enterText.split(' ').length} </div>
            </div>
            <div className="">
                <h4>Preview</h4>
                {getValue}<br />
                <img src={getimage}/>
            </div>
        </div>
    )
};

export default TextCounter;
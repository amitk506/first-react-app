function FeesItem(props){
    
    return (
        <table border="1" style={{margin: "0 auto", maxWidth: "760px", width: "100%"}}>
            <thead>
                <tr>
                    <th>Student ID</th>
                    <th>Student Name</th>
                    <th>Student Class Name</th>
                    <th>Student Fee Month</th>
                    <th>Student Fee Amount</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>               
                {
                    props.item == false
                    ? 
                    <tr>
                        <td colSpan={6} style={{textAlign: "center"}}>No Data Available</td>
                    </tr>
                    : 
                    props.item.map((itemLoop, index) => (                   
                    <tr key={index}>
                        <td>{index + 1}</td>
                        <td>{itemLoop.stuname}</td>
                        <td>{itemLoop.stuclass}</td>
                        <td>{itemLoop.stufeemonth}</td>
                        <td>{itemLoop.stufeeamount}</td>                    
                        <td><a onClick={props.deleteItem}>Delete</a></td>                    
                    </tr>
                    ))
                }
                
            </tbody>
        </table>
    )
};

export default FeesItem;
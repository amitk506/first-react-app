import React, {useState} from "react";
import AddFees from "./AddFees";
import FeesItem from "./FeesItem";

function Fees(){
    
    const [stuValue, setStuValue] = useState([]);
    
    const saveStuData = (data) => {
        setStuValue([...stuValue, data]);
    }

    const deleteItem = (index) => {
        const rowItem = [...stuValue];
        rowItem.splice(index, 1);
        setStuValue(rowItem);
    }

   return (        
        <div>
            <AddFees getStuData={saveStuData} />
            <hr />
            <FeesItem item={stuValue} deleteItem={deleteItem}/>
        </div>
    )
};

export default Fees;


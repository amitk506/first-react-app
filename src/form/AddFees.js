import React, {useState} from 'react';

import './AddFees.css';

function AddFees(props){

    const classes = ['Nursury', 'LKG', 'UKG', '1st'];
    const month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    
    const [stuInput, setStuInput] = useState({});

    const handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        setStuInput(values => ({...values, [name]: value}));
    }
    
    const StuSubmitHandler = (stoppageerefersh) => {
        stoppageerefersh.preventDefault();  

        setStuInput('');

        props.getStuData(stuInput);
        
    }
    
    return (
        <form className="feesForm" onSubmit={StuSubmitHandler}>
            <input type="text" placeholder="Enter Student Name" name="stuname" value={stuInput.stuname || ''} onChange={handleChange} required />
            <select name="stuclass"  value={stuInput.stuclass || ''} onChange={handleChange} required>
                <option value="">Select Class</option>
                {
                    classes.map(stuclass => 
                        <option key={stuclass}>{stuclass}</option>
                    )
                }
            </select>
            <select name="stufeemonth"  value={stuInput.stufeemonth || ''} onChange={handleChange} required>
                <option value="">Select Month</option>
                {
                    month.map(stufeemonth =>
                        <option key={stufeemonth}>{stufeemonth}</option>    
                    )
                }
                    
            </select>
            <input type="number" placeholder="Enter Fee Amount" name="stufeeamount"  value={stuInput.stufeeamount || ''} onChange={handleChange} required />
            <button type="submit">Submit</button>
        </form>
    )
};

export default AddFees;
const DateFormat = props => {
    const day = props.studentDateJoinAttr.toLocaleString('en-US', {day: '2-digit'});
    const month = props.studentDateJoinAttr.toLocaleString('en-US', {month: 'short'});
    const year = props.studentDateJoinAttr.getFullYear();
    
    return(
       <div className="dateformat">{day}, {month}, {year}</div>
    )
}

export default DateFormat;
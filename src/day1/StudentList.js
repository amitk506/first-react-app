import StudentAdd from './StudentAdd';
import StudentItem from './StudentItem';

const StudentList = () => {

    const studentData = [
        {
            stuKey: 1,
            stuId : "CS1",
            stuName : "Priyanka Srivastava",
            stuRolNum : 1,
            stuBranch : "Computer Science",
            stuDJoin : new Date(),
            stuFee : 1000
        },
        {
            stuKey: 2,
            stuId : "EE1",
            stuName : "Rajveer Singh",
            stuRolNum : 1,
            stuBranch : "Electronic Engg.",
            stuDJoin : new Date(),
            stuFee : 900
        },
        {
            stuKey: 3,
            stuId : "EE1",
            stuName : "Rajiv Sharma",
            stuRolNum : 1,
            stuBranch : "Electrical Engg.",
            stuDJoin : new Date(),
            stuFee : 1100
        },
        {
            stuKey: 4,
            stuId : "CS1",
            stuName : "Anvi Srivastava",
            stuRolNum : 2,
            stuBranch : "Computer Science",
            stuDJoin : new Date(),
            stuFee : 1000
        },
        {
            stuKey: 5,
            stuId : "ME1",
            stuName : "Sumit Kumar",
            stuRolNum : 1,
            stuBranch : "Mechnical Engg.",
            stuDJoin : new Date(),
            stuFee : 1100
        }
    ];    

    return(        
        <div>
            <h1>Add New Student Record</h1>
            <StudentAdd/>
            <br />
            <h1>Student Record List</h1>
            <div className='studentItemList'>
                {   
                    studentData.map(
                        studentDataLoop =>
                            <StudentItem
                                key={studentDataLoop.stuKey}
                                studentID={studentDataLoop.stuId}
                                studentName={studentDataLoop.stuName}
                                studentRollNumber={studentDataLoop.stuRolNum}
                                studentBranch={studentDataLoop.stuBranch}
                                studentDateJoin={studentDataLoop.stuDJoin}
                                studentFee={studentDataLoop.stuFee} />
                    )
                }        
            </div>  
        </div>
    )
};

export default StudentList;
import React, { useState } from 'react';

import './StudentAdd.css';

const StudentAdd = (props) => {
    
    const [enterId, setenterId] = useState('');
    const [enterName, setenterName] = useState('');
    const [enterRolnum, setenterRolnum] = useState('');
    const [enterBranch, setenterBranch] = useState('');
    const [enterDoj, setenterDoj] = useState('');
    const [enterFee, setenterFee] = useState('');

    const stuIdChangeHandler = (event) => {
        setenterId(event.target.value);
    };
    const stuNameChangeHandler = (event) => {
        setenterName(event.target.value);
    };
    const stuRolnumChangeHandler = (event) => {
        setenterRolnum(event.target.value);
    };
    const stuBranchChangeHandler = (event) => {
        setenterBranch(event.target.value);
    };
    const stuDoJChangeHandler = (event) => {
        setenterDoj(event.target.value);
    };
    const stuFeeChangeHandler = (event) => {
        setenterFee(event.target.value);
    };

    const stuSubmitHandler = (event) => {
        event.preventDefault();

        const stuRecordData = {
            stuRID: enterId,
            stuRName: enterName,
            stuRRolnum: enterRolnum,
            stuRBranch: enterBranch,
            stuRDoj: new Date(enterDoj),
            stuRFee: enterFee
        }

        props.onSaveStuSubmtData(stuRecordData);

        setenterId('');
        setenterName('');
        setenterRolnum('');
        setenterBranch('');
        setenterDoj('');
        setenterFee('');

    };

    return(
        <form onSubmit={stuSubmitHandler}>
            <div className="StuAdd-Field">
                <input type="text" value={enterId} placeholder="Enter ID" onChange={stuIdChangeHandler}/>
            </div>
            <div className="StuAdd-Field">
                <input type="text" value={enterName} placeholder="Enter Name" onChange={stuNameChangeHandler}/>
            </div>
            <div className="StuAdd-Field">
                <input type="text" value={enterRolnum} placeholder="Enter Roll Number" onChange={stuRolnumChangeHandler}/>
            </div>
            <div className="StuAdd-Field">
                <input type="text" value={enterBranch} placeholder="Enter Branch" onChange={stuBranchChangeHandler}/>
            </div>
            <div className="StuAdd-Field">
                <input type="date" value={enterDoj} placeholder="Enter Date Of Joining" onChange={stuDoJChangeHandler}/>
            </div>
            <div className="StuAdd-Field">
                <input type="number" value={enterFee} placeholder="Enter Fee" onChange={stuFeeChangeHandler}/>
            </div>
            <div className="StuAdd-Field">
                <button>Submit</button>
            </div>
        </form>
    )
};

export default StudentAdd;
import './StudentItem.css';
import DateFormat from './Dateformat';

const StudentItem = props => {
    return(
        <div className="studentItem">
            <p>{props.studentID}</p>
            <p>{props.studentName}</p>
            <p>{props.studentRollNumber}</p>
            <p>{props.studentBranch}</p>
            <DateFormat studentDateJoinAttr={props.studentDateJoin} />
            <p>${props.studentFee}</p>
        </div>
    );
}

export default StudentItem;
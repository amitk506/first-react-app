import BlogItem from './BlogItem';
import dummyImg from '.././images/dummy-img.png';

function BlogList(){

    const bloglistData = [
        {
            blogImage: dummyImg,
            blogTitle: 'Title 1',
            blogDate: 'Date 1',
            blogDescription: 'Description 1',
            blogLink: '/blog-link1',
        },
        {
            blogImage: dummyImg,
            blogTitle: 'Title 2',
            blogDate: 'Date 2',
            blogDescription: 'Description 2',
            blogLink: '/blog-link2',
        },
        {
            blogImage: dummyImg,
            blogTitle: 'Title 3',
            blogDate: 'Date 3',
            blogDescription: 'Description 3',
            blogLink: '/blog-link3',
        }

    ];

    return (
        <div className=''>
            {
                bloglistData.map((bloglistDataLoop, index) => 
                    <BlogItem 
                        key={index}
                        bImage={bloglistDataLoop.blogImage} 
                        bTitle={bloglistDataLoop.blogTitle} 
                        bDate={bloglistDataLoop.blogDate} 
                        bDesc={bloglistDataLoop.blogDescription} 
                        bLink={bloglistDataLoop.blogLink}
                    />
                )
            }
        </div>
    )
};

export default BlogList;
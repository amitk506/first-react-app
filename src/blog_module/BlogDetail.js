import { useLocation } from "react-router-dom";

function BlogDetail(){
    const location = useLocation();
    const states = location.state;
    
    return(
        <div>
            {states && (
                <div className="">
                    <div className=""><img src={states.bImage} alt={states.bTitle}/></div>
                    <div className="">{states.bTitle}</div>
                    <div className="">Published Date: {states.bDate}</div>
                    <div className="">{states.bDesc}</div>
                </div>
            )}
        </div>
    )
};

export default BlogDetail;
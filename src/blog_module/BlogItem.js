import { Link } from "react-router-dom";

const BlogItem = (props) => {
    return (
        <div className="">
            <div className=""><img src={props.bImage} alt={props.bTitle}/></div>
            <div className="">{props.bTitle}</div>
            <div className="">Published Date: {props.bDate}</div>
            <div className="">{props.bDesc}</div>
            <div className=""><Link to='blogdetail' state={props}>Read More</Link></div><br />
        </div>
    )
};

export default BlogItem;
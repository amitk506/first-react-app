import React, {useState} from "react";

import Expenses from "./components/Expenses/Expenses";
import NewExpense from "./components/NewExpense/NewExpense";

const DUMMY_EXPENSE = [
    {
        id: 1,
        date: new Date(),
        title: "Title1",
        amount: "200"
    },
    {
        id: 2,
        date: new Date(),
        title: "Title2",
        amount: "300"
    },
    {
        id: 3,
        date: new Date(),
        title: "Title3",
        amount: "400"
    }
];

function App(){

    const [expenses, setExpenses] = useState(DUMMY_EXPENSE);

    const addExpenseHandler = (expnese) => {
        const updatedExpense = [expnese, ...expenses];
        setExpenses(updatedExpense);
        console.log(expnese);
    }

    return(
        <div>
            <NewExpense onAddExpense={addExpenseHandler} />
            {
                expenses.map((expensesLoop) =>
                    <Expenses item={expensesLoop} />
                )
            }
        </div>
    )

};

export default App;
import ExpenseItem from "./ExpenseItem";

function Expenses(props){
    return(
        <ExpenseItem date={props.item.date} title={props.item.title} amount={props.item.amount} />
    )
};

export default Expenses;
import './ExpenseItem.css';
import Card from "../UI/Card";
import ExpenseDate from "./ExpenseDate";

function ExpenseItem(props){
    return(
        <Card className="custom-card">
            <ExpenseDate date={props.date}/>
            <div className='title'>{props.title}</div>
            <div className='amount'>${props.amount}</div>
        </Card>
    )
};

export default ExpenseItem;
import React from "react";
import ExpenseForm from "./ExpenseForm";

function NewExpense(props){

    const saveExpenseDataHander = (enteredExpenseData) => {
        const expenseData = {
            ...enteredExpenseData, 
            id: Math.random().toString()
        }

        props.onAddExpense(expenseData);

        console.log(expenseData);
    };

    return(
        <div className="">
            <ExpenseForm onSaveExpenseData={saveExpenseDataHander} />
        </div>
    )
};

export default NewExpense;